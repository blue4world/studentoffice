import re

from nltk.corpus import stopwords

from external_libraries.findKeywords import *
from htmlStripper import *


class ParagraphDataHolder:
    def __init__(self, rawInput):
        self.rawText = rawInput
        self.text = ""
        self.textWithoutHtml = ""
        self.isEnglish = True
        self.keywords = list()
        #self.keywords
        #self.wordlist = None
        #self.tf = { }
        #self.tfidf = { }
        self.removeHtml()
        self.checkLanguage()
        self.text = ParagraphDataHolder.clearText(self.text)
        self.removeStopWords()
        self.findKeyWords()
        #self.calculateTF()

    @staticmethod
    def clearText(text):
        text = text.replace("'s", "")
        text = text.replace("&#;", "")
        text = text.replace("%", " ")
        text = text.replace("!", " ")
        text = text.replace("?", " ")
        text = text.replace("'", "")
        text = text.replace(".", " ")
        text = text.replace("+", " ")
        text = text.replace(",", " ")
        text = text.replace(")", "")
        text = text.replace("-", " ")
        text = text.replace("(", "")
        text = text.replace(":", "")
        text = text.replace("/", "")
        text = text.replace("\"", " ")
        text = text.lower()
        text = re.sub("\d+", "", text)
        return text

    def removeStopWords(self):
        cachedStopWords = stopwords.words("english")
        cachedStopWords.append("ja")
        cachedStopWords.append("ut")
        cachedStopWords.append("co")
        cachedStopWords.append("tambet")
        cachedStopWords.append("aprill")
        cachedStopWords.append("toimub")
        cachedStopWords.append("ee")
        self.textWithoutStopWords = ' '.join([word for word in self.text.split() if word not in cachedStopWords])


    def removeHtml(self):
        s = MLStripper()
        s.feed(self.rawText)
        self.text = s.get_data()
        self.textWithoutHtml = s.get_data()

    def checkLanguage(self):
        testString = re.sub('[ -~]', '', self.getText())
        testString = re.sub("\n","",testString)
        testString = re.sub("\t","",testString)
        if len(testString) >= 1:
            self.isEnglish = False

    def getText(self):
        return self.text

    def removeKeyWords(self):
        self.keywords = list()

    def addKeyWord(self,keyword):
        self.keywords.append(keyword)

    def findKeyWords(self):
        if self.isEnglish:
            self.keywords = FindKeywords.find(self.textWithoutStopWords)

    '''
    def calculateTF(self):
        allText = self.textWithoutStopWords

        wordlist = allText.split()

        word_freq = {}
        for word in wordlist:
            word_freq[word] = word_freq.get(word, 0) + 1

        self.tf = { }
        for word in word_freq.keys():
            self.tf[word] = float(word_freq[word]) / float(len(wordlist))
    '''
