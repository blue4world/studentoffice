import requests

from external_libraries.readability import *
from paragraphDataHolder import *


class PageDataHolder:
    def __init__(self, url, userDefined = False):
        self.paragraphs = list()
        if not userDefined:
            self.url = url
            self.downloadContent()
            self.processContent()
            self.findKeyWords()
            self.userDefined = userDefined
        else:
            self.url = "UserDefined"
            self.userDefined = userDefined

    def printContent(self):
        for p in self.paragraphs:
            print p.getText()
            print "---"

    def downloadContent(self):
        #input = urllib2.urlopen(url).read().decode('utf-8')
        input = requests.get(self.url)._content
        self.readibility = Readability(input,self.url)

    def getTitle(self):
        if self.userDefined == False:
            title = self.readibility.title
            title = ParagraphDataHolder.clearText(title)
            title = title.split("|")
            return title[0]
        else:
            return "UserDefined"

    def getUrl(self):
        return self.url

    def processContent(self):
        content =  self.readibility.content.split("\n")
        for c in content:
            c = c.replace("<p>", "")
            c = c.replace("</p>", "")
            #c = unicodedata.normalize('NFKD', c).encode('ascii','ignore')
            paragraph = ParagraphDataHolder(c)
            self.paragraphs.append(paragraph)

    def getRawContent(self):
        return self.readibility.content

    def getTextWithoutStop(self):
        text = ""
        for p in self.paragraphs:
            if p.isEnglish:
                text = text+" "+p.textWithoutStopWords
        return text

    def findKeyWords(self):
        self.keywords = FindKeywords.find(self.getTextWithoutStop())







    

