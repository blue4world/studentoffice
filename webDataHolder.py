from pageDataHolder import  *
from plistCreator import *
import pickle
from userDefinedDataHolder import *
import math
from random import randint
import wikipedia

class WebDataHolder:
    def __init__(self, urls):
        self.data = list()
        #self.wordFreq = None
        #self.idf = None
        for url in urls:
            print "Processing URL: "+url
            p = PageDataHolder(url)
            self.data.append(p)

        print "Processing User Defined: "
        user = UserDefinedDataHolder()
        self.data.append(user.getPageHolder())

        self.removeDupliciteKeyword()


    def removeDupliciteKeyword(self):
        keywords = { }
        for page in self.data:
            for paragraph in page.paragraphs:
                if paragraph.isEnglish:
                    for k,v in paragraph.keywords:
                        if k in keywords:
                            if v > keywords[k]:
                                keywords[k] = v
                        else:
                            keywords[k] = v

        usedKeywords = set()
        for page in self.data:
            for paragraph in page.paragraphs:
                if paragraph.isEnglish:
                    newlist = list()
                    for tup in paragraph.keywords:
                        if tup[1] >= keywords[tup[0]]:
                            if tup[0] not in usedKeywords:
                                newlist.append(tup)
                                usedKeywords.add(tup[0])
                    paragraph.keywords = newlist


    def printTitles(self):
        for d in self.data:
                print d.getTitle()

    def findInTheTitle(self,stringToFind):
        l = list()
        for d in self.data:
            if stringToFind in d.getTitle():
                l.append(d)
        return l

    def getKeyWords(self):
        string = ""
        for page in self.data:
            string = string + page.getTitle() + ","
            for paragraph in page.paragraphs:
                if paragraph.isEnglish:
                    for keyword in paragraph.keywords:
                        string = string + str(keyword[0])+","
        return string

    def printKeyWords(self):
        for page in self.data:
            print page.getTitle()
            for paragraph in page.paragraphs:
                if paragraph.isEnglish:
                    print paragraph.keywords
            print "---"

    def getResponse(self,string):
        if "keyword" in string:
            return self.getKeyWords()

        string = string.lower()
        text = " "
        i = 0
        for pageHolder in self.data:
            for paragraph in pageHolder.paragraphs:
                if paragraph.isEnglish:
                    j = 0
                    for keyword in paragraph.keywords:
                        if keyword[0] in string:
                            j = j + 1
                    if j > i:
                        j = i
                        text = paragraph.textWithoutHtml
        if text != " ":
            text = self.upperfirst(text)
            text = text+"."
            return text
        else:
            return self.generateUnrelatedAnswer(string)


    def upperfirst(self,x):
        return x[0].upper() + x[1:]

    def generatePlist(self):
        PlistCreator(self)

    def generateUnrelatedAnswer(self,string):
        k = FindKeywords.find(string)
        if len(k) > 0:
            questionKeyword = k[0][0]
            questionKeyword = ParagraphDataHolder.clearText(questionKeyword)
            questionKeyword = questionKeyword.replace(" ", "")
            ran = (randint(0,8))
            if ran == 0:
                return "You can find information about "+str(questionKeyword)+" on the university website."
            if ran > 2:
                try:
                    output = wikipedia.summary(questionKeyword, sentences=2)
                    if output == None or output == "":
                        ran = 2
                    else:
                        return output
                except:
                    ran = 2
            if ran == 2:
                return "If you want to know more about "+str(questionKeyword)+" you should go to Student Office and ask there."
        else:
            if (randint(0,1)) == 0:
                return "Can you specify your question?"
            else:
                return "I can not answer this question"


'''
    def createCSV(self):
        with open('output.csv', 'wb') as file:
            file.write("question,text,ID\n")
            i = 0
            for page in self.data:
                string = ""
                for paragraph in page.paragraphs:
                    if paragraph.isEnglish:
                        i = i + 1
                        keywords = paragraph.keywords
                        file.write('"')
                        for word in keywords:
                            file.write(word[0]+"\n\n")
                        file.write('"'+","+'"')
                        file.write(paragraph.getText().replace("\n", " "))
                        file.write(","+'"')
                        file.write("Anybody-")
                        file.write(str(i))
                        file.write('"')
                        file.write("\n")

    def getAllText(self):
        # all text
        allText = ""
        for p in self.data:
            allText = allText + p.getText()
        return allText

    def calculateIDF(self):


        wordlist = self.getAllText().split()
        word_freq = {}
        for word in wordlist:
            word_freq[word] = word_freq.get(word, 0) + 1

        #tx = [ (v, k) for (k, v) in word_freq.items()]
        #tx.sort(reverse=True)
        #word_freq_sorted = [ (k, v) for (v, k) in tx ]


        #self.wordFreq = word_freq_sorted
        numOfDocuments = 0
        for page in self.data:
            for paragraph in page.paragraphs:
                numOfDocuments = numOfDocuments + 1


        self.idf = { }
        for word in word_freq.keys():
            self.idf[word] = math.log(float(numOfDocuments) / word_freq[word])

        #print self.idf
        #print self.wordFreq
        #print self.totalNumOfWords

    def calculateTFIDF(self):
        for page in self.data:
            for paragraph in page.paragraphs:
                if paragraph.isEnglish:
                    for word in paragraph.tf.keys():
                        if word in self.idf.keys():
                            paragraph.tfidf[word] = self.idf[word] * paragraph.tf[word]
                        else:
                            print "Cant find word: "+word
'''


