from pageDataHolder import *
from paragraphDataHolder import *

class UserDefinedDataHolder:

    def __init__(self):
        self.page = PageDataHolder("",userDefined=True)
        set = [
        ("hi","Hello"),
        ("hello","Hi"),
        ("how are you","I'm fine. Thank you"),
        ("how are you","I'm fine. Thank you"),
        ("summer semester start","Summer semester starts 8th of February and ends last wekk of June"),
        ("computer faculty","You can find Computer Science Faculty at J. Liivi street"),
        ("school open","School buildings are open every day from 6.00 to 22.00"),
        ("holidays","No holidays this semester"),
        ("what's up","No holidays this semester"),
        ("deadline course registration","It has to be done by second weeks of semester"),
        ("minimum credits semester","18 credits but you should have much more"),
        ("change study plan","Yes, it is possible. You should to visit student office and inform there"),
        ("fees","You have to go to the Office and inform there"),
        ("scholarship","You can find information about scholarships on the university website")
        ]
        for s in set:
            p = ParagraphDataHolder(s[1])
            p.removeKeyWords()
            p.addKeyWord((s[0],1))
            p.isEnglish = True
            self.page.paragraphs.append(p)


    def getPageHolder(self):
        return self.page