from webDataHolder import *
from ircClient import *
import sys

holderFilename = "data.pkl"

def initWebDataHolder(notWebUpdate = True):
        if os.path.exists(holderFilename) and notWebUpdate:
                sys.stderr.write("Loading from file...\n")
                w = loadHolder()
        else:
                sys.stderr.write("Downloading...\n")
                w = WebDataHolder(createUrlsList())
        return w

def saveHolder(dataHolder):
        with open(holderFilename, 'wb') as output:
                pickle.dump(dataHolder, output, pickle.HIGHEST_PROTOCOL)

def loadHolder():
        with open(holderFilename, 'rb') as input:
                return pickle.load(input)

def createUrlsList():

        # wget "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/ -O - | grep '<a href="/2016' | cut -d '"' -f 2-2
        urls = [
        "http://www.ut.ee/en/university",
        "http://www.ut.ee/en/courses-taught-english",
        "http://www.ut.ee/en/university/general/history",
        "http://www.ut.ee/en/university/general",
        "http://www.ut.ee/en/prospective-students/bachelors-studies",
        "http://www.ut.ee/en/prospective-students/masters-studies",
        "http://www.ut.ee/en/frequently-asked-questions",
        "http://www.ut.ee/en/prospective-students/estonian-language-studies",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/bis/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/advancedalg/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/DB/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/dbt/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/dm2016/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/dmseminar/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/turve/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/cg-project/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/cg-sem/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/neuroseminar/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/AKT/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/bioseminar/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/at/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/scml-seminar/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/enterprise/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/esi/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/GjaL/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/gnat/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/dssem/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/HLAB/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/InfDidSem/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/itkoolis/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/infoturve/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/react/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/ui/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/nlp/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/Compression/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/cryptoseminar/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/crypto1/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/ml/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/praktika/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/mcsem/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/MADP/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/multimeedia/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/mmwd/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/avmm/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/OOP/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/opsys/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/CPL/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/cloud/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/cpp/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/prog-alused/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/progmaa/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/PKS/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/proglang/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/programming-games/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/voimkond/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/appcrypto/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/access/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/digicomm/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/IITP/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/isc/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/sa/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/AO4/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/SWT2016/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/SDC/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/TIAM/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/ao3/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/secprog/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/secprog-proj/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/ssd/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/weeb/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/vl/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/vt2/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/ba/spring",
        "https://courses.cs.ut.ee/user/lang/en?redirect=https://courses.cs.ut.ee/2016/bpm/spring"
        ]


        urls = [
        "http://www.ut.ee/en/university",
        "http://www.ut.ee/en/courses-taught-english",
        "http://www.ut.ee/en/university/general/history",
        "http://www.ut.ee/en/university/general",
        "http://www.ut.ee/en/prospective-students/bachelors-studies",
        "http://www.ut.ee/en/prospective-students/masters-studies",
        "http://www.ut.ee/en/frequently-asked-questions",
        "http://www.ut.ee/en/prospective-students/estonian-language-studies",

        ]



        return urls



"""
if len(sys.argv) == 2:
        w = initWebDataHolder(notWebUpdate = True)
        print w.getResponse(str(sys.argv[1]))

else:
        w = initWebDataHolder(notWebUpdate = True)
        print "No parameters given"
        #w.printKeyWords()
        #w.generatePlist()
        #w.printTitles()

        #l = w.findInTheTitle("About")
        #about = l[0]
saveHolder(w)
"""
irc = True
w = initWebDataHolder(notWebUpdate = True)
saveHolder(w)
if irc:
        ircClient = IrcClient(w)
        ircClient.run()
else:
        #w.printTitles()
        w.generatePlist()
        #print w.getKeyWords()
        #w.printKeyWords()
        #print w.getResponse("How are you?")
        #l = w.findInTheTitle("UserDefined")
        #about = l[0]
        #print about.paragraphs[0].textWithoutHtml

