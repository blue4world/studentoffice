#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import socket
import string
import os
import platform
import time

class IrcClient:
    # Variables
    HOST = "irc.freenode.net"
    PORT = 6667
    NICK = "StudentOffice"
    IDENT = "StudentOffice"
    REALNAME = "Student Office"
    CHAN = "#tartu"

    def __init__(self,dataHolder):
        self.readbuffer = ""
        self.dataHolder = dataHolder
        self.connected = False
        self.irc = None

    def run(self):
        # Our IRC connection
        self.irc = socket.socket()
        self.irc.settimeout(300)
        self.connected = False

        self.connection(IrcClient.HOST, IrcClient.PORT, IrcClient.NICK, IrcClient.IDENT, IrcClient.REALNAME, IrcClient.CHAN)

        while self.connected:
            try:
                #data = self.irc.recv ( 4096 )
                data = self.irc.recv ( 12288 )
                # If connection is lost
                if len(data) == 0:
                    break
                print data
                # If Nick is in use
                if data.find ( "Nickname is already in use" ) != -1:
                    NICK = IrcClient.NICK + str(time.time())
                    self.connection(IrcClient.HOST, IrcClient.PORT, NICK, IrcClient.IDENT, IrcClient.REALNAME, IrcClient.CHAN)
                # Ping Pong so we don't get disconnected
                if data[0:4] == "PING":
                    self.irc.send ( "PONG " + data.split() [ 1 ] + "\r\n" )
                elif len(data) >= 3:
                    stringArray = data.split(" ")
                    print stringArray
                    string = stringArray[3:]
                    string = ' '.join(string)
                    string = string.replace(":","")
                    print "Question: "+string
                    response = (self.dataHolder.getResponse(string))
                    if response != None:
                        self.irc.send( "PRIVMSG %s :%s\r\n" % (IrcClient.CHAN, response.encode('ascii', 'ignore') ) )

                #if ":ahoj" in data:
                #    self.irc.send( "PRIVMSG %s :%s\r\n" % (IrcClient.CHAN, "ahoj" ))

            except socket.timeout:
                global connected
                connected = False
                print connected
                break
        print "Out of loop"


    def connection(self,host, port, nick, ident, realname, chan):
        while self.connected is False:
            try:
                self.irc.connect((host, port))
                self.irc.send("NICK %s\r\n" % nick)
                self.irc.send("USER %s %s bla :%s\r\n" % (ident, host, realname))
                self.irc.send("JOIN :%s\r\n" % chan)
                # Initial msg to send when bot connects
                # irc.send("PRIVMSG %s :%s\r\n" % (chan, "TehBot: "+ nick + " Realname: " + realname + " ."))
                self.connected = True
            except socket.error:
                print "Attempting to connect..."
                time.sleep(5)
                continue

