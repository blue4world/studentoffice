import plistlib
class PlistCreator:
    def __init__(self,holder):
        self.questionId = 1
        pl = dict()
        pl["question_format"] = "edu.usc.ict.npc.editor.model.processor.text.TextProcessorProvider"
        pl["answer_format"] = "edu.usc.ict.npc.editor.model.processor.text.TextProcessorProvider"
        pl["recordingQuestions"] = False


        pl["answers"] = list()
        pl["questions"] = list()
        pl["map"] = list()

        map_list = pl["map"]
        answers_list = pl["answers"]
        question_list = pl["questions"]

        # keywords
        i = 0
        for page in holder.data:
            for paragraph in page.paragraphs:
                if paragraph.isEnglish:
                    if len(paragraph.keywords) > 1:
                        answers_list.append(self.generateAnswer(paragraph.getText(),i))
                        question_list_len_before = len(question_list)
                        for keyword,n in paragraph.keywords:
                            question_list.append(self.generateQuestion(keyword))
                            for question_template in self.getQuestionlist():
                                question_list.append(self.generateQuestion(question_template+" "+keyword))
                        question_list_diff = len(question_list) -question_list_len_before
                        for num in range(0,question_list_diff):
                            map_list.append(self.generateMap(6,question_list_len_before+num,i))
                        i = i + 1

        # titles
        for page in holder.data:
            title = page.getTitle()
            question_list.append(self.generateQuestion(title))
            for paragraph in page.paragraphs:
                if paragraph.isEnglish:
                    aid = self.findAnswerByName(paragraph.getText(),answers_list)
                    if aid != -1:
                        map_list.append(self.generateMap(6,len(question_list)-1,aid))

        plistlib.writePlist(pl, "out.plist")


    def findAnswerByName(self,text,answers_list):
        i = 0
        for a in answers_list:
            question = self.generateQuestion(text)
            if question["text"] == a["text"]:
                return i
            i = i + 1
        return -1


    def generateMap(self,strength,qid,aid):
        map = dict()
        map["value"] = 4
        map["qid"] = qid
        map["aid"] = aid
        return map

    def generateQuestion(self,text):
        question = dict()
        question["text"] = text
        question["ID"] = "Anybody-"+str(self.questionId)
        self.questionId = self.questionId + 1
        return question

    def generateAnswer(self,text,id):
        answer = dict()
        answer["text"] = text
        answer["speaker"] = 0
        #answer["ID"] = "utterance_"+str(id)
        answer["ID"] = "DummySpeaker"+str(id)
        answer["tokens"] = [ 16 ]
        return answer

    def getQuestionlist(self):
        list = [
            "Tell me",
            "Tell me about",
            "I would like to know",
            "Infom me",
            "How",
            "How about",
            "Why",
            "Why are",
            "Where",
            "Where are",
            "What",
            "What are",
            "When",
            "When are"
        ]
        return list